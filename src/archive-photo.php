<?php
/**
 *
 * @package WordPress
 * @subpackage Graciya
 * @since 1.0
 * @version 1.0
 */
get_header(); ?>

<?php 
$photo__banner = get_field('photos_banner', 'option');
$image = ( $photo__banner['image'] ) ? ' style="background-image:url('.$photo__banner['image'].')"' : '';
if( $photo__banner ){ ?>
<div class="photos__banner"<?php echo $image; ?>>
    <div class="container">
        <div class="row">
            <div class="col">
                <?php if( $photo__banner['title'] ) { ?>
                <div class="content">
                    <h1><?php echo $photo__banner['title']; ?></h1>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<div class="photos__navigation">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <?php 
                if( has_nav_menu('photos-menu') ){
                    wp_nav_menu( array(
                        'theme_location'        => 'photos-menu',
                        'container'             => 'nav',
                        'container_class'       => 'photos__nav'
                    ) ); 
                } ?>    
            </div>
        </div>
    </div>
</div> 
<?php if ( have_posts() ) : ?>
<div class="photos__section">
    <div class="container">
        <div class="row">
        <?php while ( have_posts() ) : the_post(); ?>
            <div class="col-lg-6">
                <?php get_template_part( 'template-parts/photo/content'); ?>
            </div>
        <?php endwhile; ?>
        </div>
    </div>
</div>
<?php endif; ?> 
<?php get_footer();