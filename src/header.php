<?php
/**
 *
 * @package WordPress
 * @subpackage Graciya
 * @since 1.0
 * @version 1.0
 */
$header__class = ( is_front_page() ) ? ' class="transparent"' : ''; 
$main__class = ( is_front_page() || get_field('show_page_banner') || is_archive('photo') ) ? '' : ' class="padding"'; 
$social = get_field('social_links', 'option');

$wpml_lang = icl_get_languages();
$cur_lang_li = '';
$all_lang_li = '';
$lang_mobile = '';
foreach ($wpml_lang as $lang ) {
    if($lang['active']){
        $cur_lang_li = '<span>'.$lang['code'].'</span>';
    } else {
        $all_lang_li .= '<li>
                            <a href="'.$lang['url'].'" class="wpml_'.$lang['code'].'">
                                <span>'.$lang['code'].'</span>
                            </a>
                        </li>';
    }
} ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>

</head>
<div class="preloader__wrapper">
    <div class="whirlpool"></div>
</div>
<body <?php body_class(); ?>>
    <div class="burger__menu">
        <div class="additional__row">
            <?php if( get_field('logo', 'option') ) { ?>
            <a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                <img src="<?php the_field('logo', 'option'); ?>" alt="<?php echo get_bloginfo('name'); ?>">
            </a>
            <?php } ?>
            <div class="right__bar float-right">
                <?php if( get_field('phone_number', 'option') ) { ?>
                <a class="phone__number d-none d-md-block float-left" href="tel:<?php the_field('phone_number', 'option'); ?>"><?php the_field('phone_number', 'option'); ?></a>
                <?php } ?>
                <div class="lang__switcher float-left" onclick="">
                    <div class="current">
                        <?php echo $cur_lang_li; ?>
                    </div>
                    <?php if( $all_lang_li ) {?> 
                    <ul>
                        <?php echo $all_lang_li; ?>
                    </ul>
                    <?php } ?>
                </div>
                <div class="full__menu hide float-left">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
        </div>
        <div class="main__burger">
            <div class="navigation">
                <?php if( has_nav_menu('burger-main-menu') ) { 
                    wp_nav_menu( array(
                        'theme_location'        => 'burger-main-menu',
                        'container'             => 'nav',
                        'container_class'       => 'burger__main__nav float-left'
                    ) );
                } ?>
                <?php if( has_nav_menu('burger-services-menu') ) { ?>
                    <div class="show__service d-block d-md-none">
                        <span><?php _e('Services', 'graciya'); ?></span>
                        <i></i>
                    </div>
                <?php } ?>
            </div>
            <?php if( get_field('phone_number', 'option') ) { ?>
            <a class="phone__number d-block d-md-none" href="tel:<?php the_field('phone_number', 'option'); ?>"><?php the_field('phone_number', 'option'); ?></a>
            <?php } ?>
            <div class="social__block">
                <?php if( $social['messenger'] ) { ?>
                <a href="<?php echo $social['messenger']; ?>" target="_blank" class="messenger"></a>
                <?php } ?>
                <?php if( $social['viber'] ) { ?>
                <a href="tel:<?php echo $social['viber']; ?>" class="viber"></a>
                <?php } ?>
                <?php if( $social['whatsapp'] ) { ?>
                <a href="<?php echo $social['whatsapp']; ?>" target="_blank" class="whatsapp"></a>
                <?php } ?>
                <?php if( $social['facebook'] ) { ?>
                <a href="<?php echo $social['facebook']; ?>" target="_blank" class="facebook"></a>
                <?php } ?>
                <?php if( $social['instagram'] ) { ?>
                <a href="<?php echo $social['instagram']; ?>" target="_blank" class="instagram"></a>
                <?php } ?>
                <?php if( $social['youtube'] ) { ?>
                <a href="<?php echo $social['youtube']; ?>" target="_blank" class="youtube"></a>
                <?php } ?>
                <?php if( $social['telegram'] ) { ?>
                <a href="<?php echo $social['telegram']; ?>" target="_blank" class="telegram"></a>
                <?php } ?>
            </div>
        </div>
        <div class="service__burger">
            <?php if( has_nav_menu('burger-services-menu') ) { ?>
                <div class="navigation">
                    <div class="show__main d-block d-md-none">
                        <i></i>
                        <span><?php _e('To main menu', 'graciya'); ?></span>
                    </div>
                    <h5><?php _e('Services', 'graciya'); ?></h5>
                    <?php wp_nav_menu( array(
                        'theme_location'        => 'burger-services-menu',
                        'container'             => 'nav',
                        'container_class'       => 'burger__main__nav float-left'
                    ) ); ?>
                </div>
            <?php } ?>
        </div>
    </div>
    <header id="header"<?php echo $header__class; ?>>
        <div class="container-fluid">
            <div class="row">
                <div class="col">
                    <?php if( get_field('logo', 'option') ) { ?>
                    <a class="logo float-left" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <img src="<?php the_field('logo', 'option'); ?>" alt="<?php echo get_bloginfo('name'); ?>">
                    </a>
                    <?php } 
                    if( has_nav_menu('main') ){
                        wp_nav_menu( array(
                            'theme_location'        => 'main',
                            'container'             => 'nav',
                            'container_class'       => 'main__nav d-none d-lg-block float-left'
                        ) ); 
                    } ?>
                    <div class="right__bar float-right">
                        <div class="contact__block d-none d-md-block float-left">
                            <?php if( get_field('messenger', 'option') ) { ?>
                            <a href="<?php the_field('messenger', 'option'); ?>" target="_blank" class="messenger"></a>
                            <?php } ?>
                            <?php if( get_field('viber', 'option') ) { ?>
                            <a href="tel:<?php the_field('viber', 'option'); ?>" class="viber"></a>
                            <?php } ?>
                            <?php if( get_field('whatsapp', 'option') ) { ?>
                            <a href="<?php the_field('whatsapp', 'option'); ?>" target="_blank" class="whatsapp"></a>
                            <?php } ?>
                        </div>
                        <?php if( get_field('phone_number', 'option') ) { ?>
                        <a class="phone__number d-none d-sm-block float-left" href="tel:<?php the_field('phone_number', 'option'); ?>"><?php the_field('phone_number', 'option'); ?></a>
                        <?php } ?>
                        <div class="lang__switcher float-left" onclick="">
                            <div class="current">
                                <?php echo $cur_lang_li; ?>
                            </div>
                            <?php if( $all_lang_li ) {?> 
                            <ul>
                                <?php echo $all_lang_li; ?>
                            </ul>
                            <?php } ?>
                        </div>
                        <div class="full__menu float-left">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </header>
    <main<?php echo $main__class; ?>>