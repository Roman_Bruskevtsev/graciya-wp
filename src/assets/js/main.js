(function($) {
    function headerBackground(){
        if( $('header').hasClass('transparent') ){
            var scrollTop = $(window).scrollTop();
            if( scrollTop >= 100 ){
                $('header').addClass('background');
            } else {
                $('header').removeClass('background');
            }
        }
    }
    $(window).on('scroll', function(){
        headerBackground();
    });
    $(window).on('load', function(){
        headerBackground();
        $('.preloader__wrapper').addClass('hide');
        /*Burger menu*/
        $('.full__menu').on('click', function(){
            $('.burger__menu').toggleClass('show');
        });

        $('.burger__menu .show__service').on('click', function(){
            $('.service__burger').addClass('show');
        });

        $('.burger__menu .show__main ').on('click', function(){
            $('.service__burger').removeClass('show');
        });

        if($('.front__banner').length){
            $('.front__banner').slick({
                infinite:           false,
                autoplay:           true,
                autoplaySpeed:      8000,
                arrows:             false,
                speed:              700,
                dots:               true,
                fade:               false,
                slidesToShow:       1
            });
        }

        if($('.services__slider').length){
            $('.services__slider').slick({
                infinite:           false,
                autoplay:           true,
                autoplaySpeed:      8000,
                speed:              700,
                dots:               false,
                fade:               false,
                slidesToShow:       4,
                prevArrow:          $('.services__section .prev__slide'),
                nextArrow:          $('.services__section .next__slide'),
                responsive: [
                    {
                        breakpoint: 991,
                        settings: {
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 575,
                        settings: {
                            slidesToShow: 1
                        }
                    }
                ]
            });
        }
        if($('.steps__slider').length){
            $('.steps__slider').slick({
                infinite:           true,
                autoplay:           true,
                autoplaySpeed:      8000,
                speed:              800,
                dots:               false,
                fade:               false,
                slidesToShow:       5,
                prevArrow:          $('.how__it__works .prev__slide'),
                nextArrow:          $('.how__it__works .next__slide'),
                responsive: [
                    {
                        breakpoint: 1500,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 991,
                        settings: {
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 575,
                        settings: {
                            slidesToShow: 1
                        }
                    }
                ]
            });
        }
        if($('.get__slider').length){
            $('.get__slider').slick({
                infinite:           true,
                autoplay:           true,
                autoplaySpeed:      8000,
                speed:              800,
                dots:               false,
                fade:               false,
                slidesToShow:       4,
                prevArrow:          $('.get__section .prev__slide'),
                nextArrow:          $('.get__section .next__slide'),
                responsive: [
                    {
                        breakpoint: 1500,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 991,
                        settings: {
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 575,
                        settings: {
                            slidesToShow: 1
                        }
                    }
                ]
            });
        }
        if($('.results__slider').length){
            $('.results__slider').slick({
                draggable:          false,
                infinite:           true,
                autoplay:           true,
                autoplaySpeed:      8000,
                speed:              800,
                dots:               false,
                fade:               false,
                slidesToShow:       1,
                prevArrow:          $('.compare__results .prev__slide'),
                nextArrow:          $('.compare__results .next__slide')
            });
            $('.ba-slider').beforeAfter();
        }
        if($('.image__slider').length){
            $('.image__slider').slick({
                draggable:          true,
                infinite:           true,
                autoplay:           true,
                autoplaySpeed:      8000,
                speed:              800,
                dots:               false,
                fade:               false,
                slidesToShow:       1
            });
        }
        if($('.images__slider, .gallery__slider').length){
            $('.images__slider, .gallery__slider').slick({
                draggable:          true,
                infinite:           true,
                autoplay:           true,
                autoplaySpeed:      8000,
                speed:              800,
                dots:               false,
                fade:               false,
                slidesToShow:       1
            });
        }
        if($('.image__caption__slider .slider').length){
            $('.image__caption__slider .slider').slick({
                draggable:          true,
                infinite:           true,
                autoplay:           true,
                autoplaySpeed:      8000,
                speed:              800,
                dots:               false,
                fade:               false,
                slidesToShow:       1
            });
        }
        if($('.partners__slider').length){
            $('.partners__slider').slick({
                infinite:           true,
                autoplay:           true,
                autoplaySpeed:      8000,
                speed:              800,
                dots:               false,
                fade:               false,
                slidesToShow:       4,
                prevArrow:          $('.partners__section .prev__slide'),
                nextArrow:          $('.partners__section .next__slide'),
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 3
                        }
                    },
                    {
                        breakpoint: 991,
                        settings: {
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 575,
                        settings: {
                            slidesToShow: 1
                        }
                    }
                ]
            });
        }
        if($('.photos__slider').length){
            $('.photos__slider').slick({
                draggable:          true,
                infinite:           true,
                autoplay:           true,
                autoplaySpeed:      8000,
                speed:              800,
                dots:               false,
                fade:               false,
                slidesToShow:       1
            });
        }
        $('.faq .title').on('click', function(){
            var faq = $(this).closest('.faq'),
                text = faq.find('.text');
            faq.toggleClass('show');
            text.slideToggle(300);
        });
        /*Popup*/
        $('.get__service button').on('click', function(){
            var service = $('.service__title h1').text();
            $('.get__service.popup input[name=service]').val(service);

            $('.popup__wrapper').addClass('show');
            $('.get__service.popup').addClass('show');
        });
        $('.get__equipment button').on('click', function(){
            var equipment = $('.press__title h1').text();
            $('.get__equipment.popup input[name=equipment]').val(equipment);

            $('.popup__wrapper').addClass('show');
            $('.get__equipment.popup').addClass('show');
        });
        $('.close__popup').on('click', function(){
            $('.popup').removeClass('show');
            $('.popup__wrapper').removeClass('show');
        });

        /*Price tab*/
        $('.tabs__nav .tab').on('click', function(){
            var thisTab = $('.tabs__nav .tab').index(this);

            $(this).addClass('active');
            $('.tabs__nav .tab').not(this).removeClass('active');
            $('.tab__blocks .tab__block').removeClass('active');
            $('.tab__blocks .tab__block').eq(thisTab).addClass('active');
        });

        /*Isotope*/
        var $grid = $('.press__grid').isotope({
            itemSelector: '.press__item',
            layoutMode: 'fitRows'
        });

        var $postGrid = $('.post__grid').isotope({
            itemSelector: '.post__item',
            layoutMode: 'fitRows'
        });

        $('.press__navigation li').on('click', function(){
            var className = $(this).attr('class').replace(' active', '');
            $(this).addClass('active');
            $('.press__navigation li').not(this).removeClass('active');

            if(className == 'all') {
                $grid.isotope({filter: '*' });
            } else {
                $grid.isotope({filter: '.' + className });
            }
        });

        /*Load press*/
        if ( $('.press__grid').length) {
            var maxPageCount,
                cat,
                searchKey,
                page = 1;

            $('.more__posts button').on('click', function(){
                maxPageCount = parseInt($('.press__grid').data('max-page')),
                cat = $('.press__grid').data('cat'),
                searchKey = $('.press__grid').data('search');
                $(this).closest('.more__posts').addClass('load');
                $('.press__grid').addClass('load');
                $.post( ajaxurl, {
                    'action'        : 'graciya_load_press',
                    'category_id'   : cat,
                    'page'          : page,
                    'search_key'    : searchKey
                })
                .done(function(response) {
                    var $items = $(response);
                    $grid.append( $items ).isotope( 'appended', $items );
                    
                    page++;
                    if(maxPageCount <= page) {
                        $('.more__posts button').addClass('disable');
                    } else {
                        $('.more__posts button').removeClass('disable');
                    }
                    $('.press__grid').removeClass('load');
                    $('.more__posts').removeClass('load');
                });

            });
        }

        /*Load post*/
        if ( $('.post__grid').length) {
            var maxPageCount,
                cat,
                searchKey,
                page = 1;

            $('.more__posts button').on('click', function(){
                maxPageCount = parseInt($('.post__grid').data('max-page')),
                cat = $('.post__grid').data('cat'),
                searchKey = $('.post__grid').data('search');
                $(this).closest('.more__posts').addClass('load');
                $('.post__grid').addClass('load');
                $.post( ajaxurl, {
                    'action'        : 'graciya_load_post',
                    'category_id'   : cat,
                    'page'          : page,
                    'search_key'    : searchKey
                })
                .done(function(response) {
                    var $items = $(response);
                    $postGrid.append( $items ).isotope( 'appended', $items );
                    
                    page++;
                    if(maxPageCount <= page) {
                        $('.more__posts button').addClass('disable');
                    } else {
                        $('.more__posts button').removeClass('disable');
                    }
                    $('.post__grid').removeClass('load');
                    $('.more__posts').removeClass('load');
                });

            });
        }

        $(function() {
            var marker = [], infowindow = [], map, image = $('.map__wrapper').attr('data-marker');

            function addMarker(location,name,contentstr){
                marker[name] = new google.maps.Marker({
                    position: location,
                    map: map,
                    icon: image
                });
                marker[name].setMap(map);

                infowindow[name] = new google.maps.InfoWindow({
                    content:contentstr
                });
                
                google.maps.event.addListener(marker[name], 'click', function() {
                    infowindow[name].open(map,marker[name]);
                });
            }

            function initialize() {

                var lat = $('#map-canvas').attr("data-lat");
                var lng = $('#map-canvas').attr("data-lng");
                var mapStyle = googleMapStyle;

                var myLatlng = new google.maps.LatLng(lat,lng);

                var setZoom = parseInt($('#map-canvas').attr("data-zoom"));
                
                var styledMap = new google.maps.StyledMapType(mapStyle,{name: "Styled Map"});

                var mapOptions = {
                    zoom: setZoom,
                    disableDefaultUI: false,
                    scrollwheel: false,
                    zoomControl: true,
                    streetViewControl: true,
                    center: myLatlng
                };
                map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
                
                map.mapTypes.set('map_style', styledMap);
                map.setMapTypeId('map_style');
                

                $('.location__list a').each(function(){
                    var mark_lat = $(this).attr('data-lat');
                    var mark_lng = $(this).attr('data-lng');
                    var this_index = $('.location__list a').index(this);
                    var mark_name = 'template_marker_'+this_index;
                    var mark_locat = new google.maps.LatLng(mark_lat, mark_lng);
                    var mark_str = $(this).attr('data-string');;
                    addMarker(mark_locat,mark_name,mark_str);   
                });   
            }

            
            if ($('.map__wrapper').length){    
                setTimeout(function(){
                    initialize();
                }, 500);
            }
        });

        AOS.init();
    });
})(jQuery);