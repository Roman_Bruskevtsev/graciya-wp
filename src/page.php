<?php
/**
 *
 * @package WordPress
 * @subpackage Graciya
 * @since 1.0
 * @version 1.0
 */
get_header();

$show_banner = get_field('show_page_banner');
$page_banner = get_field('page_banner');

$page_section = ( $page_banner && $show_banner ) ? 'page__section banner' : 'page__section';

echo '<div class="'.$page_section.'">';
    if( $show_banner ){
        get_template_part( 'inc/acf-content/page_banner' );
    } else{
        echo '<div class="container">
                <div class="row">
                    <div class="col">
                        <h1 data-aos="fade-left">'.get_the_title().'</h1>
                    </div>
                </div>
              </div>';
    }
        
            if( have_rows('content') ):
                while ( have_rows('content') ) : the_row();
                    if( get_row_layout() == 'services_section' ):
                        get_template_part( 'inc/acf-content/services_section' );
                    elseif ( get_row_layout() == 'text_image_section' ):
                        get_template_part( 'inc/acf-content/text_image_section' );
                    elseif ( get_row_layout() == 'how_it_works_section' ):
                        get_template_part( 'inc/acf-content/how_it_works_section' );
                    elseif ( get_row_layout() == 'contact_form_section' ):
                        get_template_part( 'inc/acf-content/contact_form_section' );
                    elseif ( get_row_layout() == 'what_you_get_section' ):
                        get_template_part( 'inc/acf-content/what_you_get_section' );
                    elseif ( get_row_layout() == 'compare_results_slider' ):
                        get_template_part( 'inc/acf-content/compare_results_slider' );
                    elseif ( get_row_layout() == 'map_section' ):
                        get_template_part( 'inc/acf-content/map_section' );
                    elseif ( get_row_layout() == 'price_block' ):
                        get_template_part( 'inc/acf-content/price_block' );
                    elseif ( get_row_layout() == 'text_block_image_slider' ):
                        get_template_part( 'inc/acf-content/text_block_image_slider' );
                    elseif ( get_row_layout() == 'two_column_text' ):
                        get_template_part( 'inc/acf-content/two_column_text' );
                    elseif ( get_row_layout() == 'text_editor' ):
                        get_template_part( 'inc/acf-content/text_editor' );
                    elseif ( get_row_layout() == 'image_slider_text' ):
                        get_template_part( 'inc/acf-content/image_slider_text' );
                    elseif ( get_row_layout() == 'partner_slider' ):
                        get_template_part( 'inc/acf-content/partner_slider' );
                    elseif ( get_row_layout() == 'faq' ):
                        get_template_part( 'inc/acf-content/faq' );
                    elseif ( get_row_layout() == 'contact_block' ):
                        get_template_part( 'inc/acf-content/contact_block' );
                    elseif ( get_row_layout() == 'schedule_table' ):
                        get_template_part( 'inc/acf-content/schedule_table' );
                    elseif ( get_row_layout() == 'images_slider' ):
                        get_template_part( 'inc/acf-content/images_slider' );
                    endif;
                endwhile;
            else :
                echo '
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="no__content">
                                    <h5 data-aos="fade-left">'.__('Nothing to show', 'graciya').'</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                ';
            endif;

echo '</div>';

get_footer();