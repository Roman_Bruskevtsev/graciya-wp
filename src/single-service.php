<?php
/**
 *
 * @package WordPress
 * @subpackage Graciya
 * @since 1.0
 * @version 1.0
 */
get_header(); ?>

<div class="container">
    <div class="row">
        <div class="col-lg-10">
            <div class="service__title">
                <?php 
                $icon = get_field('icon');
                if( $icon ) { ?>
                <div class="icon">
                    <img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['title']; ?>">
                </div>
                <?php } ?>
                <h1><?php the_title(); ?></h1>
            </div>
        </div>
        <?php if( get_field('price') ) { ?>
        <div class="col-lg-2">
            <div class="service__price">
                <span><?php _e('Price:', 'graciya'); ?></span>
                <h5><?php the_field('price'); ?></h5>
            </div>
        </div>
        <?php } ?>
    </div>
    <?php if( get_field('details') ) { ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="service__details"><?php the_field('details'); ?></div>
        </div>
    </div>
    <?php } ?>
    <?php 
    $recommendation = get_field('recommendation');
    if( $recommendation ) { ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="recommendation">
                <?php echo $recommendation['text']; ?>  
            </div>
        </div>
    </div>
    <?php } ?>
    <div class="row">
    <?php if( $recommendation['column_1'] ) { ?>
        <div class="col-lg-6">
            <div class="recommendation__column">
                <?php echo $recommendation['column_1']; ?>  
            </div>
        </div>
    <?php } ?>
    <?php if( $recommendation['column_2'] ) { ?>
        <div class="col-lg-6">
            <div class="recommendation__column">
                <?php echo $recommendation['column_2']; ?>  
            </div>
        </div>
    <?php } ?>
    </div>
    <?php if( get_field('get_service_label') ) { ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="get__service text-center">
                <button class="btn simple__btn"><?php the_field('get_service_label'); ?></button>        
            </div>
        </div>
    </div>
    <?php } ?>
</div>
<?php get_template_part( 'template-parts/service/content-compare'); ?>

<?php get_footer();