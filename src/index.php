<?php
/**
 *
 * @package WordPress
 * @subpackage Graciya
 * @since 1.0
 * @version 1.0
 */
get_header(); ?>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="blog__title" data-aos="fade-left">
                <h1><?php single_post_title(); ?></h1>
            </div>
        </div>
    </div>
</div>
<?php 
$posts_per_page = (int) get_option('posts_per_page');

$all_posts_args = array(
    'posts_per_page'        => $posts_per_page,
    'orderby'               => 'date',
    'post_status'           => 'publish',
    'post_type'             => 'post'
);

$all_query = new WP_Query( $all_posts_args );
$max_pages = (int) $all_query->max_num_pages;

if ( $all_query->have_posts() ) { ?>
<div class="container">
    <div class="row post__grid" data-page="1" data-max-page="<?php echo $max_pages; ?>" data-cat="*">
    <?php while ( $all_query->have_posts() ) { $all_query->the_post();
        get_template_part( 'template-parts/post/content');
    } ?>
    </div>
    <?php if ( $max_pages > 1) { ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="more__posts text-center">
                <button class="btn simple__btn"><?php _e('More news', 'graciya'); ?></button>
                <div class="load__icon"></div>
            </div>
        </div>
    </div>
    <?php } ?>
</div>
<?php } wp_reset_postdata();  ?>

<?php get_footer();