<?php
/**
 *
 * @package WordPress
 * @subpackage Graciya
 * @since 1.0
 * @version 1.0
 */
get_header(); ?>

<?php if( get_field('services_archive_title','option') ){ ?>
<div class="container">
    <div class="row">
        <div class="col">
            <div class="page__title" data-aos="fade-left">
                <h1><?php the_field('services_archive_title','option'); ?></h1>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<?php if ( have_posts() ) : ?>
<div class="container">
    <div class="row">
    <?php while ( have_posts() ) : the_post(); ?>
        <div class="col-md-6 col-lg-3">
            <?php get_template_part( 'template-parts/service/content'); ?>
        </div>
    <?php endwhile; ?>
    </div>
</div>
<?php endif; ?>
<?php get_footer();