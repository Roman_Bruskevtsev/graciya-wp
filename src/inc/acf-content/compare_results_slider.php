<section class="compare__results">
    <div class="container">
        <?php if( get_sub_field('title') ) { ?>
        <div class="row">
            <div class="col">
                <div class="section__title">
                    <h2><?php the_sub_field('title'); ?></h2>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php if( have_rows('slider') ) { ?>
        <div class="row">
            <div class="col">
                <div class="result__wrapper">
                    <div class="result__nav">
                        <span class="prev__slide"></span>
                        <span class="next__slide"></span>
                    </div>
                    <div class="results__slider">
                        <?php 
                        while ( have_rows('slider') ) : the_row(); 
                            $image_before = get_sub_field('before_image');
                            $image_after = get_sub_field('after_image');
                            $slider = '';
                            if( $image_before && $image_after ) $slider = ' ba-slider';
                        ?>
                        <div class="slide">
                            <div class="result__block">
                                <div class="images<?php echo $slider; ?>">
                                    <?php if( $image_before ) { ?>
                                    <img src="<?php echo $image_before['url']; ?>" alt="<?php echo $image_before['title']; ?>">
                                    <?php } ?>
                                    <?php if( $image_after ) { ?>
                                    <div class="resize">
                                        <img src="<?php echo $image_after['url']; ?>" alt="<?php echo $image_after['title']; ?>">
                                    </div>
                                    <span class="handle"></span>
                                    <?php } ?>
                                    <?php if( get_sub_field('diagnosis') ) { ?>
                                    <span class="diagnosis"><?php the_sub_field('diagnosis'); ?></span>
                                    <?php } ?>
                                </div>
                                <div class="content">
                                    <?php if( get_sub_field('comments') ) { ?>
                                    <p><?php the_sub_field('comments'); ?></p>
                                    <?php } ?>
                                    <?php if( get_sub_field('patient') ) { ?>
                                    <h6><?php the_sub_field('patient'); ?></h6>
                                    <?php } ?>
                                    <?php if( get_sub_field('status') ) { ?>
                                    <span><?php the_sub_field('status'); ?></span>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <?php endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php if( get_sub_field('button_link') ){ ?>
            <div class="row">
                <div class="col">
                    <div class="page__link text-center" data-aos="fade-up">
                        <a href="<?php echo get_sub_field('button_link'); ?>" class="btn simple__btn"><?php the_sub_field('button_label'); ?></a>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</section>