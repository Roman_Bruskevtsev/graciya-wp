<section class="map__section">
    <div class="map__block">
        <div class="map__wrapper" id="map-canvas"
             data-lat="<?php the_sub_field('latitude'); ?>"
             data-lng="<?php the_sub_field('longitude'); ?>"
             data-zoom="<?php the_sub_field('zoom'); ?>"
             data-marker="<?php the_sub_field('marker'); ?>">
        </div>
        <?php 
        $contact_block = get_sub_field('contact_block');
        $form_block = get_sub_field('form_block');
        if( $contact_block['title'] || $contact_block['address'] || $contact_block['phones'] || $contact_block['email'] ) { ?>
        <div class="contact__block">
            <?php if( $contact_block['title'] ) { ?><h6><?php echo $contact_block['title']; ?></h6><?php } ?>
            <?php if( $contact_block['address'] ) { ?><div class="address"><?php echo $contact_block['address']; ?></div><?php } ?>
            <?php $phones = $contact_block['phones'];
            if( $phones ) { ?>
                <div class="phones">
                <?php foreach ($phones as $phone) { ?>
                    <a href="tel:<?php echo $phone['tel']; ?>"><?php echo $phone['tel']; ?></a>
                <?php } ?>
                </div>
            <?php } ?>
            <?php if( $contact_block['email'] ) { ?><div class="email"><?php echo $contact_block['email']; ?></div><?php } ?>
        </div>
        <?php } 
        if( $form_block ) { ?>
        <div class="form__block">
            <?php if( $form_block['title'] ) { ?><h6><?php echo $form_block['title']; ?></h6><?php } ?>
            <?php if( $form_block['form_shortcode'] ) echo do_shortcode( $form_block['form_shortcode'] ); ?>
        </div>
        <?php } ?>
        <div class="location__list">
            <a class="marker" data-rel="map-canvas" data-lat="<?php the_sub_field('marker_latitude'); ?>"
             data-lng="<?php the_sub_field('marker_longitude'); ?>" data-string="<?php the_sub_field('marker_string'); ?>"></a>
        </div>

    </div>
</section>