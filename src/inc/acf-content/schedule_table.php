<?php if( have_rows('tab') ): ?>
<div class="table__row">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="price__tabs" data-aos="fade-up">
                    <div class="tabs__nav">
                    <?php $i = 0;
                    while ( have_rows('tab') ) : the_row(); ?>
                        <div class="tab tab__<?php echo $i; if($i == 0) echo ' active'; ?>">
                            <span><?php the_sub_field('title'); ?></span>
                        </div>
                    <?php 
                    $i++;
                    endwhile; ?>
                    </div>
                    <div class="tab__blocks">
                        <?php $j = 0;
                        while ( have_rows('tab') ) : the_row(); ?>
                            <div class="tab__block tab__<?php echo $j; if($j == 0) echo ' active'; ?>">
                                <?php if( get_sub_field('description') ){ ?>
                                <div class="tab__description">
                                    <p><?php the_sub_field('description'); ?></p>
                                </div>
                                <?php } ?>
                                <?php if( have_rows('services') ): ?>
                                <div class="tab__head">
                                    <div class="tab__row">
                                        <div class="tab__column title"></div>
                                        <div class="tab__column">
                                            <span><?php _e('Monday', 'graciya'); ?></span>
                                        </div>
                                        <div class="tab__column">
                                            <span><?php _e('Tuesday', 'graciya'); ?></span>
                                        </div>
                                        <div class="tab__column">
                                            <span><?php _e('Wednesday', 'graciya'); ?></span>
                                        </div>
                                        <div class="tab__column">
                                            <span><?php _e('Thursday', 'graciya'); ?></span>
                                        </div>
                                        <div class="tab__column">
                                            <span><?php _e('Friday', 'graciya'); ?></span>
                                        </div>
                                        <div class="tab__column">
                                            <span><?php _e('Saturday', 'graciya'); ?></span>
                                        </div>
                                        <div class="tab__column">
                                            <span><?php _e('Sunday', 'graciya'); ?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab__body">
                                    <?php while ( have_rows('services') ) : the_row(); ?>
                                    <div class="tab__row">
                                        <div class="tab__column title"><span><?php the_sub_field('title'); ?></span></div>
                                        <div class="tab__column"><span><?php the_sub_field('monday_time'); ?></span></div>
                                        <div class="tab__column"><span><?php the_sub_field('thuesday_time'); ?></span></div>
                                        <div class="tab__column"><span><?php the_sub_field('wednesday_time'); ?></span></div>
                                        <div class="tab__column"><span><?php the_sub_field('thursday_time'); ?></span></div>
                                        <div class="tab__column"><span><?php the_sub_field('friday_time'); ?></span></div>
                                        <div class="tab__column"><span><?php the_sub_field('saturday_time'); ?></span></div>
                                        <div class="tab__column"><span><?php the_sub_field('sunday_time'); ?></span></div>
                                    </div>
                                    <?php endwhile; ?>
                                </div>
                                <?php endif; ?>
                            </div>
                        <?php 
                        $j++;
                        endwhile; ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if( get_sub_field('button_link') ){ ?>
            <div class="row">
                <div class="col">
                    <div class="page__link text-center" data-aos="fade-up">
                        <a href="<?php echo get_sub_field('button_link'); ?>" class="btn simple__btn"><?php the_sub_field('button_label'); ?></a>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<?php endif; ?>