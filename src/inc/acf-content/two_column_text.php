<section class="two__column__text">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            <?php if( get_sub_field('title') ) { ?>
                <h3><?php the_sub_field('title'); ?></h3>
            <?php } ?>
            <?php if( get_sub_field('text') ) { ?>
                <div class="content"><?php the_sub_field('text'); ?></div>
            <?php } ?>
            </div>
        </div>
    </div>
</section>