<?php if( get_sub_field('text') ) { ?>
<div class="content__editor">
    <?php the_sub_field('text'); ?>
</div>
<?php } ?>