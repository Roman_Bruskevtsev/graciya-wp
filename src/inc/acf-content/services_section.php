<?php 
$args = array(
    'post_type'     => 'service',
    'post_status'   => 'publish',
    'order'         => 'ASC'
);
$query = new WP_Query( $args ); ?>
<section class="services__section">
    <div class="container">
        <?php if( get_sub_field('title') ) { ?>
        <div class="row">
            <div class="col">
                <div class="section__title">
                    <h2><?php the_sub_field('title'); ?></h2>
                    <?php if ( $query->have_posts() ) { ?>
                    <div class="services__nav">
                        <span class="prev__slide"></span>
                        <span class="next__slide"></span>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php }
        if ( $query->have_posts() ) { ?>
            <div class="row">
                <div class="col">
                    <div class="services__slider">
                        <?php while ( $query->have_posts() ) { $query->the_post(); ?>
                            <div class="slide">
                                <?php get_template_part( 'template-parts/service/content'); ?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php if( get_sub_field('button_label') ){ ?>
            <div class="row">
                <div class="col">
                    <div class="archive__link text-center" data-aos="fade-up">
                        <a href="<?php echo get_post_type_archive_link( 'service' ); ?>" class="btn simple__btn"><?php the_sub_field('button_label'); ?></a>
                    </div>
                </div>
            </div>
            <?php } ?>
        <?php } wp_reset_postdata(); ?>
    </div>
</section>