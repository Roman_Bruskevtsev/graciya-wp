<?php 
$page_banner = get_field('page_banner');
$image = ( $page_banner['image'] ) ? ' style="background-image: url('.$page_banner['image'].');"' : '';
?>
<div class="page__banner"<?php echo $image; ?>>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="content">
                <?php if( $page_banner['title'] ){ ?>
                    <h1><?php echo $page_banner['title']; ?></h1>
                <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>