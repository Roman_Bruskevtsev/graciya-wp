<?php
$images = get_sub_field('images');
if( $images ) { ?>
<div class="images__slider">
<?php foreach ( $images as $image ) { ?>
    <div class="silde">
        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>">
    </div>
<?php } ?>
</div>
<?php } ?>