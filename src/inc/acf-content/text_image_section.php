<section class="text__image__section">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="content">
                <?php if( get_sub_field('title') ) { ?>
                    <h2><?php the_sub_field('title'); ?></h2>
                <?php }
                if( get_sub_field('text') ) { ?>
                    <p><?php the_sub_field('text'); ?></p>
                <?php } 
                if( get_sub_field('link_url') ) { ?>
                    <a href="<?php the_sub_field('link_url'); ?>" class="btn white__btn"><?php the_sub_field('link_label'); ?></a>
                <?php } ?>
                </div>
            </div>
            <?php if( get_sub_field('image') ) { $image = get_sub_field('image'); ?>
            <div class="col-lg-6">
                <div class="image">
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>">
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</section>