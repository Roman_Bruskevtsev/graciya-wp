<?php if( have_rows('slider') ): ?>
<section class="front__banner">
    <?php while ( have_rows('slider') ) : the_row(); 
        $background = ( get_sub_field('image') ) ? ' style="background-image: url('.get_sub_field('image')['url'].')"' : '';
    ?>
    <div class="slide"<?php echo $background; ?>>
        <div class="container">
            <div class="row">
                <div class="col-lg-7">
                    <div class="front__content">
                        <div class="content" data-aos="fade-up">
                            <?php if( get_sub_field('title') ) { ?>
                                <h1><?php echo get_sub_field('title'); ?></h1>
                            <?php } ?>
                            <?php if( get_sub_field('text') ) { ?>
                                <p><?php echo get_sub_field('text'); ?></p>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endwhile; ?>
</section>
<?php endif; ?>