<section class="how__it__works">
    <div class="container">
        <?php if( get_sub_field('title') ) { ?>
        <div class="row">
            <div class="col">
                <div class="section__title">
                    <h2><?php the_sub_field('title'); ?></h2>
                    <div class="steps__nav">
                        <span class="prev__slide"></span>
                        <span class="next__slide"></span>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
    <?php
    if( have_rows('steps') ) { ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col">
                <div class="steps__slider">
                    <?php 
                    $i = 1;
                    while ( have_rows('steps') ) : the_row(); ?>
                    <div class="slide">
                        <div class="content">
                            <span class="step">
                                <span class="circle"></span>
                                <span class="number"><?php echo $i; ?></span>
                            </span>
                            <?php if( get_sub_field('title') ) { ?>
                            <h6><?php the_sub_field('title'); ?></h6>
                            <?php }
                            if( get_sub_field('description') ) { ?>
                            <p><?php the_sub_field('description'); ?></p>
                            <?php } ?>
                        </div>
                    </div>
                    <?php 
                    $i++;
                    endwhile; ?>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
</section>