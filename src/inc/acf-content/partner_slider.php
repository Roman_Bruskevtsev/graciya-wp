<section class="partners__section">
    <div class="container">
        <?php if( get_sub_field('title') ){ ?>
        <div class="row">
            <div class="col">
                <div class="section__title">
                    <h2><?php the_sub_field('title'); ?></h2>
                    <?php if ( get_sub_field('slider') ) { ?>
                    <div class="partners__nav">
                        <span class="prev__slide"></span>
                        <span class="next__slide"></span>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php } 
        if( have_rows('slider') ): ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="partners__slider">
                <?php while ( have_rows('slider') ) : the_row();
                    $avatar = ( get_sub_field('image') ) ? ' style="background-image: url('.get_sub_field('image').')"' : '';
                    ?>
                    <div class="slide">
                        <div class="partner__block">
                            <div class="avatar"<?php echo $avatar; ?>></div>
                            <h6><?php the_sub_field('title'); ?></h6>
                            <p><?php the_sub_field('description'); ?></p>
                        </div>
                    </div>
                <?php endwhile; ?>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
</section>