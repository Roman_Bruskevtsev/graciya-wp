<section class="image__slider__text">
    <div class="container">
    <?php if( get_sub_field('title') ) { ?>
        <div class="row">
            <div class="col-lg-6">
                <h2><?php the_sub_field('title'); ?></h2>
            </div>
        </div>
    <?php } ?>
        <div class="row">
            <div class="col-lg-6">
                <?php 
                $images = get_sub_field('images');
                if( $images ) { ?>
                    <div class="image__caption__slider">
                        <div class="slider">
                        <?php foreach ($images as $image) { ?>
                            <div class="slide">
                                <div class="content">
                                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>">
                                </div>
                            </div>
                        <?php } ?>
                        </div>
                        <?php if( get_sub_field('caption') ) { ?>
                            <div class="caption"><?php the_sub_field('caption'); ?></div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
            <div class="col-lg-6">
            <?php if( get_sub_field('text') ) { ?>
                <div class="text"><?php the_sub_field('text'); ?></div>
            <?php } ?>
            </div>
        </div>
    </div>
</section>