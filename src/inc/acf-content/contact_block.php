<div class="container">
    <div class="row">
        <div class="col-lg-6">
            <div class="contacts__block">
                <?php if( get_sub_field('address') ) { ?>
                    <div class="address"><?php the_sub_field('address'); ?></div>
                <?php } ?>
                <?php $phones = get_sub_field('phones'); 
                    if( $phones ) { ?>
                    <div class="phones">
                        <?php foreach ($phones as $phone) { ?>
                            <a href="tel:<?php echo $phone['tel']; ?>"><?php echo $phone['tel']; ?></a>
                        <?php } ?>
                    </div>
                <?php } ?>
                <?php if( get_sub_field('email') ) { ?>
                    <div class="email">
                        <a href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('email'); ?></a>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="contacts__block">
                <?php if( get_sub_field('text') ) { ?>
                    <div class="text"><?php the_sub_field('text'); ?></div>
                <?php } ?>
                <?php if( get_sub_field('title') ) { ?>
                    <div class="title"><h6><?php the_sub_field('title'); ?></h6></div>
                <?php } ?>
                <?php $add_phones = get_sub_field('additional_phones');
                    if( $add_phones ) { ?>
                    <div class="additional__phones">
                        <?php foreach ($add_phones as $phone) { ?>
                            <a href="tel:<?php echo $phone['tel']; ?>"><?php echo $phone['tel']; ?></a>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>