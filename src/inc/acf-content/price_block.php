<?php if( have_rows('tab') ): ?>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="price__tabs" data-aos="fade-up">
                <div class="tabs__nav">
                <?php $i = 0;
                while ( have_rows('tab') ) : the_row(); ?>
                    <div class="tab tab__<?php echo $i; if($i == 0) echo ' active'; ?>">
                        <span><?php the_sub_field('title'); ?></span>
                    </div>
                <?php 
                $i++;
                endwhile; ?>
                </div>
                <div class="tab__blocks">
                    <?php $j = 0;
                    while ( have_rows('tab') ) : the_row(); 
                        $number_session = get_sub_field('show_number_of_session');
                        $price_session = get_sub_field('show_price_for_one_session');
                        $price_complex = get_sub_field('show_price_for_complex');
                         ?>
                        <div class="tab__block tab__<?php echo $j; if($j == 0) echo ' active'; ?>">
                            <?php if( have_rows('services') ): ?>
                            <div class="tab__head">
                                <div class="tab__row">
                                    <div class="tab__column title"></div>
                                    <?php if( $number_session ) { ?>
                                    <div class="tab__column">
                                        <span><?php _e('Amount of session', 'graciya'); ?></span>
                                    </div>
                                    <?php } ?>
                                    <?php if( $price_session ) { ?>
                                    <div class="tab__column">
                                        <span><?php _e('Single price', 'graciya'); ?></span>
                                    </div>
                                    <?php } ?>
                                    <?php if( $price_complex ) { ?>
                                    <div class="tab__column">
                                        <span><?php _e('Complex price', 'graciya'); ?></span>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="tab__body">
                                <?php while ( have_rows('services') ) : the_row(); ?>
                                <div class="tab__row">
                                    <div class="tab__column title">
                                        <span><?php the_sub_field('title'); ?></span>
                                    </div>
                                    <?php if( $number_session ) { ?>
                                    <div class="tab__column">
                                        <span><?php the_sub_field('number_of_sessions'); ?></span>
                                    </div>
                                    <?php } ?>
                                    <?php if( $price_session ) { ?>
                                    <div class="tab__column">
                                        <span><?php the_sub_field('price_for_1_session'); ?></span>
                                    </div>
                                    <?php } ?>
                                    <?php if( $price_complex ) { ?>
                                    <div class="tab__column">
                                        <span><?php the_sub_field('price_for_complex_of_session'); ?></span>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php endwhile; ?>
                            </div>
                            <?php endif; ?>
                        </div>
                    <?php 
                    $j++;
                    endwhile; ?>
                </div>
            </div>
        </div>
    </div>
    <?php if( get_sub_field('button_link') ){ ?>
        <div class="row">
            <div class="col">
                <div class="page__link text-center" data-aos="fade-up">
                    <a href="<?php echo get_sub_field('button_link'); ?>" class="btn simple__btn"><?php the_sub_field('button_label'); ?></a>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
<?php endif; ?>