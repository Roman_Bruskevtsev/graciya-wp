<?php
$images = get_sub_field('images'); 
if( $images ) { 
?>
<section class="gallery__section">
    <div class="container">
        <?php if( get_sub_field('title') ) { ?>
        <div class="row">
            <div class="col">
                <div class="section__title">
                    <h2><?php the_sub_field('title'); ?></h2>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="row">
            <div class="col">
                <div class="gallery__slider">
                    <?php foreach ($images as $image) { ?>
                        <div class="slide">
                            <div class="content">
                                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>">
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php } ?>