<section class="text__editor">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <?php if( get_sub_field('text') ) { ?>
                <div class="content"><?php the_sub_field('text'); ?></div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>