<section class="faq__section">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
            <?php if( have_rows('faq') ) { ?>
                <div class="faq__block">
                <?php while ( have_rows('faq') ) : the_row(); ?>
                    <div class="faq__content">
                        <?php if( get_sub_field('title_1') ){ ?>
                        <div class="faq">
                            <div class="title">
                                <h6><?php the_sub_field('title_1'); ?></h6>
                                <span></span>
                            </div>
                            <?php if( get_sub_field('text_1') ){ ?>
                            <div class="text"><?php the_sub_field('text_1'); ?></div>
                            <?php } ?>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="faq__content">
                        <?php if( get_sub_field('title_2') ){ ?>
                        <div class="faq">
                            <div class="title">
                                <h6><?php the_sub_field('title_2'); ?></h6>
                                <span></span>
                            </div>
                            <?php if( get_sub_field('text_2') ){ ?>
                            <div class="text"><?php the_sub_field('text_2'); ?></div>
                            <?php } ?>
                        </div>
                        <?php } ?>
                    </div>
                <?php endwhile; ?>
                </div>
            <?php } ?>
            </div>
        </div>
        <?php if( get_sub_field('button_link') ){ ?>
            <div class="row">
                <div class="col">
                    <div class="page__link text-center" data-aos="fade-up">
                        <a href="<?php echo get_sub_field('button_link'); ?>" class="btn simple__btn"><?php the_sub_field('button_label'); ?></a>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</section>