<section class="get__section">
    <div class="container">
        <?php if( get_sub_field('title') ) { ?>
        <div class="row">
            <div class="col">
                <div class="section__title">
                    <h2><?php the_sub_field('title'); ?></h2>
                    <div class="gets__nav">
                        <span class="prev__slide"></span>
                        <span class="next__slide"></span>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php if( have_rows('list') ) { ?>
        <div class="row">
            <div class="col">
                <div class="get__slider">
                    <?php 
                    $i = 1;
                    while ( have_rows('list') ) : the_row(); ?>
                    <div class="slide">
                        <div class="get__block">
                            <div class="content">
                                <span class="step">
                                    <span class="circle"></span>
                                    <span class="number"><?php echo $i; ?></span>
                                </span>
                                <?php if( get_sub_field('title') ) { ?>
                                <h6><?php the_sub_field('title'); ?></h6>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php 
                    $i++;
                    endwhile; ?>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php if( get_sub_field('button_url') ){ ?>
            <div class="row">
                <div class="col">
                    <div class="page__link text-center" data-aos="fade-up">
                        <a href="<?php echo get_sub_field('button_url'); ?>" class="btn simple__btn"><?php the_sub_field('button_label'); ?></a>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</section>