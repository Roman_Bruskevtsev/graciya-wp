<section class="contact__form__section violet">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="content">
                    <?php if( get_sub_field('title') ) { ?>
                        <h5><?php the_sub_field('title'); ?></h5>
                    <?php } ?>
                    <?php if( get_sub_field('text') ) { ?>
                        <p><?php the_sub_field('text'); ?></p>
                    <?php } ?>
                </div>
            </div>
            <div class="col-lg-1"></div>
            <div class="col-lg-5"><?php if( get_sub_field('form_shortcode') ) echo do_shortcode( get_sub_field('form_shortcode') ); ?></div>
        </div>
    </div>
</section>