<section class="text__image__slider">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <?php if( get_sub_field('text') ) { ?>
                <div class="text">
                    <?php the_sub_field('text'); ?>
                </div>
                <?php } ?>
            </div>
            <div class="col-lg-6">
                <?php 
                $images = get_sub_field('images');
                if( $images ) { ?>
                <div class="image__slider">
                    <?php foreach ( $images as $image ) { ?>
                        <div class="slide">
                            <div class="content">
                                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>">
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>