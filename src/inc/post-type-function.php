<?php
add_action('init', 'graciya_service_post', 0);

function graciya_service_post() {
  //Register new post type
  $projects_labels = array(
  'name'                => __('Services', 'graciya'),
  'add_new'             => __('Add Service', 'graciya')
  );

  $projects_args = array(
  'label'               => __('Services', 'graciya'),
  'description'         => __('Service information page', 'graciya'),
  'labels'              => $projects_labels,
  'supports'            => array( 'title', 'thumbnail'),
  'taxonomies'          => array( '' ),
  'hierarchical'        => false,
  'public'              => true,
  'show_ui'             => true,
  'show_in_menu'        => true,
  'has_archive'         => true,
  'can_export'          => true,
  'show_in_nav_menus'   => true,
  'publicly_queryable'  => true,
  'exclude_from_search' => false,
  'query_var'           => true,
  'rewrite'             => '',
  'capability_type'     => 'post',
  'menu_icon'           => 'dashicons-star-empty'
  );
  register_post_type( 'service', $projects_args );
}

add_action('init', 'graciya_press_post', 0);

function graciya_press_post() {
  //Register new post type
  $projects_labels = array(
  'name'                => __('Press', 'graciya'),
  'add_new'             => __('Add Press', 'graciya')
  );

  $projects_args = array(
  'label'               => __('All press', 'graciya'),
  'description'         => __('Press information page', 'graciya'),
  'labels'              => $projects_labels,
  'supports'            => array( 'title', 'editor', 'excerpt', 'post-formats'),
  'taxonomies'          => array( '' ),
  'hierarchical'        => false,
  'public'              => true,
  'show_ui'             => true,
  'show_in_menu'        => true,
  'has_archive'         => true,
  'can_export'          => true,
  'show_in_nav_menus'   => true,
  'publicly_queryable'  => true,
  'exclude_from_search' => false,
  'query_var'           => true,
  'rewrite'             => '',
  'capability_type'     => 'post',
  'menu_icon'           => 'dashicons-align-left'
  );
  register_post_type( 'press', $projects_args );
}

add_action('init', 'graciya_photo_post', 0);

function graciya_photo_post() {
  $taxonomy_labels = array(
    'name'                        => __('Photos categories','graciya'),
    'singular_name'               => __('Photo category','graciya'),
    'menu_name'                   => __('Photos categories','graciya'),
  );

  $taxonomy_rewrite = array(
    'slug'                  => 'photo-categories',
    'with_front'            => true,
    'hierarchical'          => true,
  );

  $taxonomy_args = array(
    'labels'              => $taxonomy_labels,
    'hierarchical'        => true,
    'public'              => true,
    'show_ui'             => true,
    'show_admin_column'   => true,
    'show_in_nav_menus'   => true,
    'show_tagcloud'       => true,
    'rewrite'             => $taxonomy_rewrite,
  );
  register_taxonomy( 'photo-categories', 'photo', $taxonomy_args );

  //Register new post type
  $projects_labels = array(
  'name'                => __('Photos', 'graciya'),
  'add_new'             => __('Add Photo', 'graciya')
  );

  $projects_args = array(
  'label'               => __('All photos', 'graciya'),
  'description'         => __('Photo information page', 'graciya'),
  'labels'              => $projects_labels,
  'supports'            => array( 'title', 'thumbnail'),
  'taxonomies'          => array( '' ),
  'hierarchical'        => false,
  'public'              => true,
  'show_ui'             => true,
  'show_in_menu'        => true,
  'has_archive'         => true,
  'can_export'          => true,
  'show_in_nav_menus'   => true,
  'publicly_queryable'  => true,
  'exclude_from_search' => false,
  'query_var'           => true,
  'rewrite'             => '',
  'capability_type'     => 'post',
  'menu_icon'           => 'dashicons-format-gallery'
  );
  register_post_type( 'photo', $projects_args );
}

function graciya_equipment_post() {
  //Register new post type
  $projects_labels = array(
  'name'                => __('Equipments', 'graciya'),
  'add_new'             => __('Add Equipment', 'graciya')
  );

  $projects_args = array(
  'label'               => __('All equipment', 'graciya'),
  'description'         => __('Equipment information page', 'graciya'),
  'labels'              => $projects_labels,
  'supports'            => array( 'title', 'editor' , 'thumbnail'),
  'taxonomies'          => array( '' ),
  'hierarchical'        => false,
  'public'              => true,
  'show_ui'             => true,
  'show_in_menu'        => true,
  'has_archive'         => true,
  'can_export'          => true,
  'show_in_nav_menus'   => true,
  'publicly_queryable'  => true,
  'exclude_from_search' => false,
  'query_var'           => true,
  'rewrite'             => '',
  'capability_type'     => 'post',
  'menu_icon'           => 'dashicons-marker'
  );
  register_post_type( 'equipment', $projects_args );
}

add_action('init', 'graciya_equipment_post', 0);