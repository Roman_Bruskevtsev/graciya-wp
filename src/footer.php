<?php
/**
 *
 * @package WordPress
 * @subpackage Graciya
 * @since 1.0
 * @version 1.0
 */
?>  
    </main>
    <footer>
        <div class="contact__block">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="content">
                            <?php if( get_field('contact_title', 'option') ) { ?>
                                <h5><?php the_field('contact_title', 'option'); ?></h5>
                            <?php } ?>
                            <?php if( get_field('contact_text', 'option') ) { ?>
                                <p><?php the_field('contact_text', 'option'); ?></p>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-lg-1"></div>
                    <div class="col-lg-5"><?php if( get_field('contact_form_shortcode', 'option') ) echo do_shortcode( get_field('contact_form_shortcode', 'option') ); ?></div>
                </div>
            </div>
        </div>
        <div class="footer__line">
            <div class="container">
                <div class="row">
                    <div class="col-xl-1 col-lg-2">
                        <?php if( get_field('footer_logo', 'option') ) { ?>
                        <a class="logo float-left" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                            <img src="<?php the_field('footer_logo', 'option'); ?>" alt="<?php echo get_bloginfo('name'); ?>">
                        </a>
                        <?php } ?>
                    </div>
                    <div class="col-xl-1 d-none d-xl-block"></div>
                    <div class="col-xl-6 col-lg-10">
                    <?php 
                    if( has_nav_menu('footer-menu') ){
                        wp_nav_menu( array(
                            'theme_location'        => 'footer-menu',
                            'container'             => 'nav',
                            'container_class'       => 'footer__nav'
                        ) );
                    } ?>
                    </div>
                    <?php 
                    $social = get_field('social_links', 'option');
                    if( $social ) { ?>
                        <div class="col-xl-4">
                            <div class="social__block">
                                <?php if( $social['title'] ) { ?><h6><?php echo $social['title']; ?></h6><?php } ?>
                                <?php if( $social['messenger'] ) { ?>
                                <a href="<?php echo $social['messenger']; ?>" target="_blank" class="messenger"></a>
                                <?php } ?>
                                <?php if( $social['viber'] ) { ?>
                                <a href="tel:<?php echo $social['viber']; ?>" class="viber"></a>
                                <?php } ?>
                                <?php if( $social['whatsapp'] ) { ?>
                                <a href="<?php echo $social['whatsapp']; ?>" target="_blank" class="whatsapp"></a>
                                <?php } ?>
                                <?php if( $social['facebook'] ) { ?>
                                <a href="<?php echo $social['facebook']; ?>" target="_blank" class="facebook"></a>
                                <?php } ?>
                                <?php if( $social['instagram'] ) { ?>
                                <a href="<?php echo $social['instagram']; ?>" target="_blank" class="instagram"></a>
                                <?php } ?>
                                <?php if( $social['youtube'] ) { ?>
                                <a href="<?php echo $social['youtube']; ?>" target="_blank" class="youtube"></a>
                                <?php } ?>
                                <?php if( $social['telegram'] ) { ?>
                                <a href="<?php echo $social['telegram']; ?>" target="_blank" class="telegram"></a>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="row">
                    <div class="col-xl-8 col-lg-12">
                        <?php if( get_field('copyright', 'option') ) { ?>
                        <div class="copyright">
                            <p><?php the_field('copyright', 'option'); ?></p>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="col-lg-4"></div>
                </div>
            </div>
        </div>
    </footer>
    <div class="popup__wrapper">
        <div class="get__service popup">
            <span class="close__popup"></span>
            <?php 
            $service = get_field('get_service_form', 'option');
            if( $service['title'] ) { ?>
                <h6 class="text-center"><?php echo $service['title']; ?></h6>
            <?php } 
            if( $service['form_shortcode'] ) echo do_shortcode( $service['form_shortcode'] );
            ?>
        </div>
        <div class="get__equipment popup">
            <span class="close__popup"></span>
            <?php 
            $equipment = get_field('get_equipment_form', 'option');
            if( $equipment['title'] ) { ?>
                <h6 class="text-center"><?php echo $equipment['title']; ?></h6>
            <?php } 
            if( $equipment['form_shortcode'] ) echo do_shortcode( $equipment['form_shortcode'] );
            ?>
        </div>
    </div>
    <?php wp_footer(); ?>
    <script type="text/javascript" src="https://w196759.yclients.com/widgetJS" charset="UTF-8"></script>
</body>
</html>