<?php
/**
 *
 * @package WordPress
 * @subpackage Graciya
 * @since 1.0
 * @version 1.0
 */
$background = ( get_the_post_thumbnail( get_the_ID() ) ) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID(), 'full' ).');"' : '';
get_header(); ?>

<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-lg-10">
            <div class="nav__row">
                <a href="<?php echo get_post_type_archive_link( 'equipment' ); ?>">
                    <i></i>
                    <span><?php _e('To all equipments', 'graciya'); ?></span>
                </a>
            </div>
            <div class="press__title">
                <h1><?php the_title(); ?></h1>
            </div>
            <div class="equipment__row">
                <div class="row">
                    <?php if( get_the_post_thumbnail( get_the_ID() ) ) { ?>
                    <div class="col-lg-6">
                        <?php echo get_the_post_thumbnail( get_the_ID() ); ?>
                    </div>
                    <?php } ?>
                    <div class="col-lg-6">
                        <div class="description"><?php the_content(); ?></div>
                        <div class="get__equipment">
                            <button class="btn simple__btn to__order"><?php _e('Order', 'graciya'); ?></button>
                        </div>
                    </div>
                </div>
                <?php if( get_field('details') ){ ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="details"><?php the_field('details'); ?></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="page__link text-center">
                                <div class="get__equipment">
                                    <button class="btn simple__btn to__order"><?php _e('Order', 'graciya'); ?></button>
                                </div>
                            </div> 
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="nav__row">
                <a href="<?php echo get_post_type_archive_link( 'equipment' ); ?>">
                    <i></i>
                    <span><?php _e('To all equipments', 'graciya'); ?></span>
                </a>
            </div>
        </div>
    </div>
    <?php 
    $all_posts_args = array(
        'posts_per_page'        => 3,
        'orderby'               => 'rand',
        'post_status'           => 'publish',
        'post_type'             => 'equipment',
        'post__not_in'          => array(get_the_ID())
    );

    $all_query = new WP_Query( $all_posts_args );

    if ( $all_query->have_posts() ) { ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="more__rand__press">
                <h2><?php _e('Similar equipments', 'graciya'); ?></h2>
            </div>
        </div>
    </div>
    <div class="row">
        <?php while ( $all_query->have_posts() ) { $all_query->the_post(); ?>
            <div class="col-lg-4">
                <?php get_template_part( 'template-parts/equipment/content'); ?>
            </div>
        <?php } ?>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="page__link text-center">
                <a href="<?php echo get_post_type_archive_link( 'equipment' ); ?>" class="btn simple__btn"><?php _e('To all equipments', 'graciya'); ?>
                </a>
            </div>
        </div>
    </div>
    <?php } wp_reset_postdata();  ?>
</div>

<?php get_footer();