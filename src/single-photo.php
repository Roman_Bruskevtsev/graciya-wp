<?php
/**
 *
 * @package WordPress
 * @subpackage Graciya
 * @since 1.0
 * @version 1.0
 */
get_header(); ?>

<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-lg-10">
            <div class="nav__row">
                <a href="<?php echo get_post_type_archive_link( 'photo' ); ?>">
                    <i></i>
                    <span><?php _e('To all photos', 'graciya'); ?></span>
                </a>
            </div>
            <div class="press__title">
                <h1><?php the_title(); ?></h1>
            </div>
            <?php if( have_rows('photos') ) { ?>
            <div class="photos__slider">
            <?php while ( have_rows('photos') ) : the_row(); ?>
                <div class="slide">
                    <div class="content">
                        <div class="block block__1">
                            <div class="caption">
                            <?php if( get_sub_field('caption_1') ) { ?>
                                <h6><?php the_sub_field('caption_1'); ?></h6>
                            <?php } ?>
                            </div>
                            <div class="image">
                                <?php 
                                $image_1 = get_sub_field('image_1');
                                if( $image_1 ) { ?>
                                    <img src="<?php echo $image_1['url']; ?>" alt="<?php echo $image_1['title']; ?>">
                                <?php } ?>
                            </div>
                        </div>
                        <div class="block block__2">
                            <div class="caption">
                            <?php if( get_sub_field('caption_2') ) { ?>
                                <h6><?php the_sub_field('caption_2'); ?></h6>
                            <?php } ?>
                            </div>
                            <div class="image">
                                <?php 
                                $image_2 = get_sub_field('image_2');
                                if( $image_2 ) { ?>
                                    <img src="<?php echo $image_2['url']; ?>" alt="<?php echo $image_2['title']; ?>">
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
            </div>
            <?php } ?>
            <div class="nav__row">
                <a href="<?php echo get_post_type_archive_link( 'photo' ); ?>">
                    <i></i>
                    <span><?php _e('To all photos', 'graciya'); ?></span>
                </a>
            </div>
        </div>
    </div>
    <?php 
    $all_posts_args = array(
        'posts_per_page'        => 2,
        'orderby'               => 'rand',
        'post_status'           => 'publish',
        'post_type'             => 'photo',
        'post__not_in'          => array(get_the_ID())
    );

    $all_query = new WP_Query( $all_posts_args );

    if ( $all_query->have_posts() ) { ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="more__rand__press">
                <h2><?php _e('Similar photos', 'graciya'); ?></h2>
            </div>
        </div>
    </div>
    <div class="row">
        <?php while ( $all_query->have_posts() ) { $all_query->the_post(); ?>
            <div class="col-lg-6">
                <?php get_template_part( 'template-parts/photo/content'); ?>
            </div>
        <?php } ?>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="page__link text-center">
                <a href="<?php echo get_post_type_archive_link( 'photo' ); ?>" class="btn simple__btn"><?php _e('To all photos', 'graciya'); ?>
                </a>
            </div>
        </div>
    </div>
    <?php } wp_reset_postdata();  ?>
</div>

<?php get_footer();