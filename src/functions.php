<?php
/**
 *
 * @package WordPress
 * @subpackage Graciya
 * @since 1.0
 */

/*Service post type*/
require get_template_directory() . '/inc/post-type-function.php';

/*Translation*/
require get_template_directory() . '/inc/translation.php';

/*ACF Import*/
require get_template_directory() . '/inc/acf-import.php';

/*Custom Widgets*/
// require get_template_directory() . '/inc/widgets-custom.php';

/*Theme setup*/
function graciya_setup() {
    load_theme_textdomain( 'graciya' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'post-formats', array( 'video', 'audio' ) );

    // add_image_size( 'gallery', 300, 300, true );
    // add_image_size( 'portfolio-thumbnail', 650, 330, true );
    // add_image_size( 'portfolio-medium', 825, 464, true );

    register_nav_menus( array(
        'main'                      => __( 'Main Menu', 'graciya' ),
        'footer-menu'               => __( 'Footer Menu', 'graciya' ),
        'photos-menu'               => __( 'Photos Menu', 'graciya' ),
        'burger-main-menu'          => __( 'Burger Main Menu', 'graciya' ),
        'burger-services-menu'      => __( 'Burger Services Menu', 'graciya' )
    ) );
}
add_action( 'after_setup_theme', 'graciya_setup' );

/*graciya styles and scripts*/
function graciya_scripts() {
    $version = '1.0.14';

    wp_enqueue_style( 'graciya-css', get_theme_file_uri( '/assets/css/main.min.css' ), '', $version );
    wp_enqueue_style( 'graciya-style', get_stylesheet_uri() );

    wp_enqueue_script( 'slick-js', get_theme_file_uri( '/assets/js/slick.min.js' ), array( 'jquery' ), $version, true );
    wp_enqueue_script( 'mousewheel-smoothscroll-js', get_theme_file_uri( '/assets/js/mousewheel-smoothscroll.min.js' ), array( 'jquery' ), $version, true );
    wp_enqueue_script( 'google-maps-key', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDOqauekT_r4JzPY18FOWN4N8DFR3hXo1U&libraries=places', array( 'jquery' ), $version, true );
    if( is_single() ) {
        wp_enqueue_script( 'share-js', '//platform-api.sharethis.com/js/sharethis.js#property=5c229b99c276020011d38284&product=custom-share-buttons', array('jquery'), $version, true );
        wp_enqueue_script( 'lightgallery-js', get_theme_file_uri( '/assets/js/lightgallery.min.js' ), array( 'jquery' ), $version, true );
    }
    wp_enqueue_script( 'isotope-js', get_theme_file_uri( '/assets/js/isotope.pkgd.min.js' ), array( 'jquery' ), $version, true );
    wp_enqueue_script( 'aos-js', get_theme_file_uri( '/assets/js/aos.min.js' ), array( 'jquery' ), $version, true );
    wp_enqueue_script( ' -after-js', get_theme_file_uri( '/assets/js/before-after.min.js' ), array( 'jquery' ), $version, true );
    wp_enqueue_script( 'script-js', get_theme_file_uri( '/assets/js/main.min.js' ), array( 'jquery' ), $version, true );

    if( get_field( 'google_map_style', 'option' ) ) wp_add_inline_script('script-js', 'var googleMapStyle = '.get_field( 'google_map_style', 'option' ).'', 'before' );
}
add_action( 'wp_enqueue_scripts', 'graciya_scripts' );

/*Theme settings*/
if( function_exists('acf_add_options_page') ) {
    $general = acf_add_options_page(array(
        'page_title'    => 'Theme General Settings',
        'menu_title'    => 'Theme Settings',
        'redirect'      => false,
        'capability'    => 'edit_posts',
        'menu_slug'     => 'theme-settings',
    ));
}

/*Disable guttenberg editor*/
add_filter( 'use_block_editor_for_post', '__return_false' );

/*Register sidebar*/
function graciya_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Footer 1', 'graciya' ),
        'id'            => 'footer-1',
        'description'   => __( 'Add widgets here to appear in your footer.', 'graciya' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h6>',
        'after_title'   => '</h6>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Footer 2', 'graciya' ),
        'id'            => 'footer-2',
        'description'   => __( 'Add widgets here to appear in your footer.', 'graciya' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h6>',
        'after_title'   => '</h6>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Footer 3', 'graciya' ),
        'id'            => 'footer-3',
        'description'   => __( 'Add widgets here to appear in your footer.', 'graciya' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h6>',
        'after_title'   => '</h6>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Footer 4', 'graciya' ),
        'id'            => 'footer-4',
        'description'   => __( 'Add widgets here to appear in your footer.', 'graciya' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h6>',
        'after_title'   => '</h6>',
    ) );
}
add_action( 'widgets_init', 'graciya_widgets_init' );

/*SVG support*/
function graciya_svg_types_to_uploads($file_types){
    $new_filetypes = array();
    $new_filetypes['svg'] = 'image/svg+xml';
    $file_types = array_merge($file_types, $new_filetypes );
    return $file_types;
}

add_action('upload_mimes', 'graciya_svg_types_to_uploads');

function graciya_svg_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'graciya_svg_types');

/*graciya ajax*/
function graciya_global_var() {
?>
    <script type="text/javascript">
        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
        <?php if( get_field( 'api_key', 'option' ) ) { ?>
            var apiKey = '<?php the_field( 'api_key', 'option' ); ?>';
        <?php } ?>
    </script>
<?php
}
add_action('wp_footer','graciya_global_var');

function graciya_archive_query( $query ){
    if( ! is_admin()
        && $query->is_post_type_archive( 'service' )
        && $query->is_main_query() ){
            $query->set( 'posts_per_page', -1 );
    }
    if( ! is_admin()
        && $query->is_post_type_archive( 'photo' )
        && $query->is_main_query() ){
            $query->set( 'posts_per_page', -1 );
    }
    if( ! is_admin()
        && $query->is_post_type_archive( 'equipment' )
        && $query->is_main_query() ){
            $query->set( 'posts_per_page', -1 );
    }
}
add_action( 'pre_get_posts', 'graciya_archive_query' );

/*Load posts*/
if (!function_exists('graciya_load_press')) {
    add_action( 'wp_ajax_graciya_load_press', 'graciya_load_press' );
    add_action( 'wp_ajax_nopriv_graciya_load_press', 'graciya_load_press' );

    function graciya_load_press(){
        if( !isset($_POST['category_id']) ) wp_die('id_false');
        $posts_per_page = (int) get_option('posts_per_page');
        $page = $_POST['page'];
        $catID = $_POST['category_id'];
        $searchKey = $_POST['search_key'];

        if( isset($page) ){
            $posts_offset = $page * $posts_per_page;
        }

        $args = array(
            'posts_per_page'        => $posts_per_page,
            'orderby'               => 'date',
            'post_status'           => 'publish',
            'post_type'             => 'press'
        );

        if( isset($posts_offset) ){
            $args['offset'] = $posts_offset;
        }

        if( $catID !== '*' ){
            $args['cat'] = $catID;
        }

        if( isset($searchKey) ){
            $args['s'] = $searchKey;
            $args['post_type'] = 'press';
        }

        $query = new WP_Query( $args );
        $max_pages = $query->max_num_pages;

        if ( $query->have_posts() ) :
            while ( $query->have_posts() ) : $query->the_post();
                get_template_part( 'template-parts/press/content' );
            endwhile;
        endif;
        wp_reset_postdata(); 
        wp_die();
    }
}

if (!function_exists('graciya_load_post')) {
    add_action( 'wp_ajax_graciya_load_post', 'graciya_load_post' );
    add_action( 'wp_ajax_nopriv_graciya_load_post', 'graciya_load_post' );

    function graciya_load_post(){
        if( !isset($_POST['category_id']) ) wp_die('id_false');
        $posts_per_page = (int) get_option('posts_per_page');
        $page = $_POST['page'];
        $catID = $_POST['category_id'];
        $searchKey = $_POST['search_key'];

        if( isset($page) ){
            $posts_offset = $page * $posts_per_page;
        }

        $args = array(
            'posts_per_page'        => $posts_per_page,
            'orderby'               => 'date',
            'post_status'           => 'publish',
            'post_type'             => 'post'
        );

        if( isset($posts_offset) ){
            $args['offset'] = $posts_offset;
        }

        if( $catID !== '*' ){
            $args['cat'] = $catID;
        }

        if( isset($searchKey) ){
            $args['s'] = $searchKey;
            $args['post_type'] = 'post';
        }

        $query = new WP_Query( $args );
        $max_pages = $query->max_num_pages;

        if ( $query->have_posts() ) :
            while ( $query->have_posts() ) : $query->the_post();
                get_template_part( 'template-parts/post/content' );
            endwhile;
        endif;
        wp_reset_postdata(); 
        wp_die();
    }
}