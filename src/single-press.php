<?php
/**
 *
 * @package WordPress
 * @subpackage Graciya
 * @since 1.0
 * @version 1.0
 */
get_header(); ?>

<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-lg-10">
            <div class="nav__row">
                <a href="<?php echo get_post_type_archive_link( 'press' ); ?>">
                    <i></i>
                    <span><?php _e('To all press', 'graciya'); ?></span>
                </a>
            </div>
            <?php if( get_field('subtitle') ) { ?>
                <div class="subtitle__row">
                    <span><?php the_field('subtitle'); ?></span>
                </div>
            <?php } ?>
            <div class="press__title">
                <h1><?php the_title(); ?></h1>
            </div>
            <div class="press__content"><?php the_content(); ?></div>
            <div class="nav__row">
                <a href="<?php echo get_post_type_archive_link( 'press' ); ?>">
                    <i></i>
                    <span><?php _e('To all press', 'graciya'); ?></span>
                </a>
            </div>
        </div>
    </div>
    <?php 
    $all_posts_args = array(
        'posts_per_page'        => 2,
        'orderby'               => 'rand',
        'post_status'           => 'publish',
        'post_type'             => 'press',
        'post__not_in'          => array(get_the_ID())
    );

    $all_query = new WP_Query( $all_posts_args );

    if ( $all_query->have_posts() ) { ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="more__rand__press">
                <h2><?php _e('More from the press', 'graciya'); ?></h2>
            </div>
        </div>
    </div>
    <div class="row">
        <?php while ( $all_query->have_posts() ) { $all_query->the_post();
            get_template_part( 'template-parts/press/content');
        } ?>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="page__link text-center">
                <a href="<?php echo get_post_type_archive_link( 'press' ); ?>" class="btn simple__btn"><?php _e('To all press', 'graciya'); ?>
                </a>
            </div>
        </div>
    </div>
    <?php } wp_reset_postdata();  ?>
</div>

<?php get_footer();