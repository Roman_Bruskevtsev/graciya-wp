<?php
/**
 *
 * @package WordPress
 * @subpackage Graciya
 * @since 1.0
 * @version 1.0
 */
get_header(); 

if( get_field('slider') ) get_template_part( 'inc/acf-content/front_banner' );
if( have_rows('content') ):
    while ( have_rows('content') ) : the_row();
        if( get_row_layout() == 'services_section' ):
            get_template_part( 'inc/acf-content/services_section' );
        elseif ( get_row_layout() == 'text_image_section' ):
            get_template_part( 'inc/acf-content/text_image_section' );
        elseif ( get_row_layout() == 'how_it_works_section' ):
            get_template_part( 'inc/acf-content/how_it_works_section' );
        elseif ( get_row_layout() == 'contact_form_section' ):
            get_template_part( 'inc/acf-content/contact_form_section' );
        elseif ( get_row_layout() == 'what_you_get_section' ):
            get_template_part( 'inc/acf-content/what_you_get_section' );
        elseif ( get_row_layout() == 'compare_results_slider' ):
            get_template_part( 'inc/acf-content/compare_results_slider' );
        elseif ( get_row_layout() == 'map_section' ):
            get_template_part( 'inc/acf-content/map_section' );
        elseif ( get_row_layout() == 'price_block' ):
            get_template_part( 'inc/acf-content/price_block' );
        endif;
    endwhile;
else :
    echo '
        <section class="page__section">
            <div class="page__content">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="no__content">
                                <h5 data-aos="fade-left">'.__('Nothing to show', 'graciya').'</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    ';
endif;

get_footer();