<?php
/**
 *
 * @package WordPress
 * @subpackage Graciya
 * @since 1.0
 * @version 1.0
 */
$background = ( get_the_post_thumbnail( get_the_ID() ) ) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID(), 'full' ).');"' : '';
$blog_page = (int) get_option('page_for_posts');
get_header(); ?>

<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-lg-10">
            <div class="nav__row">
                <a href="<?php echo get_permalink($blog_page); ?>">
                    <i></i>
                    <span><?php _e('To all posts', 'graciya'); ?></span>
                </a>
            </div>
            <div class="press__title">
                <h1><?php the_title(); ?></h1>
            </div>
            <div class="post__content">
                <?php 
                if( have_rows('content') ):
                    while ( have_rows('content') ) : the_row();
                        if( get_row_layout() == 'content_editor' ):
                            get_template_part( 'inc/acf-content/content_editor' );
                        elseif ( get_row_layout() == 'image_slider' ):
                            get_template_part( 'inc/acf-content/image_slider' );
                        endif;
                    endwhile;
                else :
                    echo '
                        <div class="no__content">
                            <h5 data-aos="fade-left">'.__('Nothing to show', 'graciya').'</h5>
                        </div>
                    ';
                endif; ?>
            </div>
            <div class="share__block">
                <span><?php _e('Share:', 'graciya'); ?></span>
                <div class="facebook st-custom-button" data-network="facebook" data-short-url="<?php the_permalink();?>" data-title="<?php the_title(); ?>" data-description="<?php echo strip_tags(get_the_excerpt()); ?>" data-image="<?php echo get_the_post_thumbnail_url(); ?>"></div>
                <div class="messenger st-custom-button" data-network="messenger" data-short-url="<?php the_permalink();?>" data-title="<?php the_title(); ?>" data-description="<?php echo strip_tags(get_the_excerpt()); ?>" data-image="<?php echo get_the_post_thumbnail_url(); ?>"></div>
            </div>
            <div class="nav__row">
                <a href="<?php echo get_permalink($blog_page); ?>">
                    <i></i>
                    <span><?php _e('To all posts', 'graciya'); ?></span>
                </a>
            </div>
        </div>
    </div>
    <?php 
    $all_posts_args = array(
        'posts_per_page'        => 2,
        'orderby'               => 'rand',
        'post_status'           => 'publish',
        'post_type'             => 'post',
        'post__not_in'          => array(get_the_ID())
    );

    $all_query = new WP_Query( $all_posts_args );

    if ( $all_query->have_posts() ) { ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="more__rand__press">
                <h2><?php _e('Other news', 'graciya'); ?></h2>
            </div>
        </div>
    </div>
    <div class="row">
        <?php while ( $all_query->have_posts() ) { $all_query->the_post(); ?>
            <?php get_template_part( 'template-parts/post/content'); ?>
        <?php } ?>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="page__link text-center">
                <a href="<?php echo get_permalink($blog_page); ?>" class="btn simple__btn"><?php _e('To all posts', 'graciya'); ?>
                </a>
            </div>
        </div>
    </div>
    <?php } wp_reset_postdata();  ?>
</div>

<?php get_footer();