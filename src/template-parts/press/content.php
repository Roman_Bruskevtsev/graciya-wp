<?php 
$format = get_post_format( get_the_ID() ); 
$format_class = ' standart';
if( $format == 'audio' ){
    $format_class = ' audio';
} elseif ( $format == 'video' ){
    $format_class = ' video';
} ?>
<div class="col-lg-6 press__item<?php echo $format_class; ?>">
    <a class="press__block<?php echo $format_class; ?>" href="<?php the_permalink(); ?>">
        <div class="details">
            <?php if( get_field('subtitle') ) { ?><span><?php the_field('subtitle'); ?></span><?php } ?>
        </div>
        <h6><?php the_title(); ?></h6>
        <?php the_excerpt(); ?>
    </a>
</div>