<?php
$background = ( get_the_post_thumbnail( get_the_ID() ) ) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID(), 'full' ).');"' : '';
?>
<div class="col-lg-6 post__item">
    <a href="<?php the_permalink(); ?>" class="post__block">
        <div class="thumbnail"<?php echo $background; ?>></div>
        <div class="description">
            <span class="date"><?php echo get_the_date(); ?></span>
            <h6><?php the_title(); ?></h6>
            <?php the_excerpt(); ?>
        </div>
    </a>
</div>