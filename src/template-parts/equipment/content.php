<?php
$background = ( get_the_post_thumbnail( get_the_ID() ) ) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID(), 'full' ).');"' : '';
?>
<a class="equipment__block" href="<?php the_permalink(); ?>">
    <div class="thumbnail"<?php echo $background; ?>></div>
    <div class="title">
        <h6><?php the_title(); ?></h6>
    </div>
</a>
