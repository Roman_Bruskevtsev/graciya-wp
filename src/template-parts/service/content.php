<?php 
$background = ( get_the_post_thumbnail( get_the_ID() ) ) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID(), 'full' ).');"' : '';
?>
<a class="service__block" href="<?php the_permalink(); ?>"<?php echo $background; ?>>
    <?php 
    $icon = get_field('icon');
    if( $icon ) { ?>
    <div class="icon">
        <img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['title']; ?>">
    </div>
    <?php } ?>
    <div class="content">
        <h6><?php the_title(); ?></h6>
        <?php if( get_field('description') ) { ?>
        <p><?php the_field('description'); ?></p>
        <?php } ?>
    </div>
</a>