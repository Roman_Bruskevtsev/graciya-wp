<?php
$compare = get_field('compare_results_slider');
if( $compare ) { ?>
<div class="compare__results">
    <div class="container">
        <?php if( $compare['title'] ) { ?>
        <div class="row">
            <div class="col">
                <div class="section__title">
                    <h2><?php echo $compare['title'] ; ?></h2>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php 
        $slider = $compare['slider'];
        
        if( $slider ) { ?>
        <div class="row">
            <div class="col">
                <div class="result__wrapper">
                    <div class="result__nav">
                        <span class="prev__slide"></span>
                        <span class="next__slide"></span>
                    </div>
                    <div class="results__slider">
                        <?php 
                        foreach ( $slider as $slide ) {
                            $image_before = $slide['before_image'];
                            $image_after = $slide['after_image'];
                        ?>
                        <div class="slide">
                            <div class="result__block">
                                <div class="images ba-slider">
                                    <?php if( $image_before ) { ?>
                                    <img src="<?php echo $image_before['url']; ?>" alt="<?php echo $image_before['title']; ?>">
                                    <?php } ?>
                                    <?php if( $image_after ) { ?>
                                    <div class="resize">
                                        <img src="<?php echo $image_after['url']; ?>" alt="<?php echo $image_after['title']; ?>">
                                    </div>
                                    <span class="handle"></span>
                                    <?php } ?>
                                    <?php if( $slide['diagnosis'] ) { ?>
                                    <span class="diagnosis"><?php echo $slide['diagnosis']; ?></span>
                                    <?php } ?>
                                </div>
                                <div class="content">
                                    <?php if( $slide['comments'] ) { ?>
                                    <p><?php echo $slide['comments']; ?></p>
                                    <?php } ?>
                                    <?php if( $slide['patient'] ) { ?>
                                    <h6><?php echo $slide['patient']; ?></h6>
                                    <?php } ?>
                                    <?php if( $slide['status'] ) { ?>
                                    <span><?php echo $slide['status']; ?></span>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php if( $compare['button_link'] ){ ?>
            <div class="row">
                <div class="col">
                    <div class="page__link text-center" data-aos="fade-up">
                        <a href="<?php echo $compare['button_link']; ?>" class="btn simple__btn"><?php echo $compare['button_label']; ?></a>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<?php } ?>