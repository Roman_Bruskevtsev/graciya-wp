<?php
/**
 *
 * @package WordPress
 * @subpackage Graciya
 * @since 1.0
 * @version 1.0
 */
get_header(); ?>

<?php 
$photo__banner = get_field('equipments_banner', 'option');
$image = ( $photo__banner['image'] ) ? ' style="background-image:url('.$photo__banner['image'].')"' : '';
if( $photo__banner ){ ?>
<div class="photos__banner"<?php echo $image; ?>>
    <div class="container">
        <div class="row">
            <div class="col">
                <?php if( $photo__banner['title'] ) { ?>
                <div class="content">
                    <h1><?php echo $photo__banner['title']; ?></h1>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<?php if ( have_posts() ) : ?>
<div class="photos__section padding">
    <div class="container">
        <div class="row">
        <?php while ( have_posts() ) : the_post(); ?>
            <div class="col-lg-4">
                <?php get_template_part( 'template-parts/equipment/content'); ?>
            </div>
        <?php endwhile; ?>
        </div>
    </div>
</div>
<?php endif; ?> 
<?php get_footer();