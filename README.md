# Wordpress package

For start work with project you must install:

* composer (php composer-setup.php --install-dir=bin --filename=composer)
* node.js
* npm (npm install npm@latest -g)

## For start project write in command line:
* cd graciya-wp
* composer install
* npm install

For staging used command "cp  config/staging-config.php build/config"

## For update package write in command line:
composer update

## For install plugin write in command line:
php composer.phar require "wpackagist-plugin/plugin-name" : "№ version"

## For remove plugin write in command line:
php composer.phar remove "wpackagist-plugin/plugin-name"

## For build project write in command line:
gulp

## For watch project write in command line:
gulp watch