'use strict';
var themeName   = 'graciya',
    gulp        = require('gulp'),
    watch       = require('gulp-watch'),
    babel       = require('gulp-babel'),
    sass        = require('gulp-sass'),
    cssmin      = require('gulp-cssmin'),
    jsmin       = require('gulp-minify'),
    imagemin    = require('gulp-imagemin'),
    rename      = require('gulp-rename'),
    rigger      = require('gulp-rigger'),
    browserSync = require('browser-sync'),
    concat      = require('gulp-concat'),
    buildFolder = 'build/wp-content/themes/' + themeName,
    buildPlugin = 'build/plugins',
    reload      = browserSync.reload;

gulp.task('styles', function () {
    gulp.src('src/assets/sass/**/*.scss')
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest(buildFolder + '/assets/css'))
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(buildFolder + '/assets/css/'));    
    gulp.src('src/style.css')
        .pipe(gulp.dest(buildFolder));
});

gulp.task('scripts', function () {
    gulp.src('src/assets/js/*.js')
        .pipe(jsmin({
            ext:{
                src:'-debug.js',
                min:'.min.js'
            },
            exclude: ['tasks'],
            ignoreFiles: ['.combo.js', '-min.js']
        }))
        .pipe(gulp.dest(buildFolder + '/assets/js/'));
        
});

gulp.task('php', function () {
    gulp.src('src/**/**/*.php')
        .pipe(gulp.dest(buildFolder))
        .pipe(reload({stream: true}));
});

gulp.task('images', function() {
    gulp.src('src/assets/images/**/*')
        .pipe(gulp.dest(buildFolder + '/assets/images'));
    gulp.src('src/screenshot.png')
        .pipe(gulp.dest(buildFolder));
});

gulp.task('fonts', function() {
    gulp.src('src/assets/fonts/**/*')
        .pipe(gulp.dest(buildFolder + '/assets/fonts'));
});

gulp.task('default', [
    'styles',
    'scripts',
    'php',
    'images',
    'fonts'
]);

gulp.task('watch', function () {
    gulp.watch('src/assets/sass/**/*.scss', ['styles']);

    gulp.watch('src/assets/js/**/*.js', ['scripts']);

    gulp.watch('src/**/*.php', ['php']);

    gulp.watch('src/assets/images/**/*', ['images']);
});